To create apm packages list: `apm list --installed --bare > my_package.txt`

To restore packages: `apm install --packages-file my_package.txt`
